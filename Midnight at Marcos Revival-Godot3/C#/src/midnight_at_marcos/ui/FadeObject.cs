using Godot;

namespace MidnightatMarcosRevival.midnight_at_marcos.ui
{
    public class FadeObject : ColorRect
    {
        private Tween tween;
        private Timer timer;
        
        [Export] private float fadeTime = 2;
        [Export] private float transitionTime = 2;
        [Export] private bool autoAdvance = true;
        [Export] private PackedScene nextScene;
        private Color fillColor = Colors.Black;
        private Color emptyColor;

        private int stage;

        public override void _Ready()
        {
            Visible = true;
            tween = GetNode<Tween>("Tween");
            timer = GetNode<Timer>("Timer");
            
            tween.CreateTween().TweenProperty(this, new NodePath("color"), emptyColor, fadeTime);
        }

        public override void _Process(float delta)
        {
            if (Color == emptyColor && stage == 0 && autoAdvance)
            {
                stage += 1;
                timer.Start(transitionTime);
            }
            else if (Color == fillColor && stage == 2)
            {
                GetTree().ChangeSceneTo(nextScene);
            }
        }

        public void onTimerTimeout()
        {
            stage += 1;
            tween.CreateTween().TweenProperty(this, new NodePath("color"), fillColor, fadeTime);
        }
    }
}
