using Godot;

namespace MidnightatMarcosRevival.midnight_at_marcos.ui
{
    public class Menu : Control
    {
        public HBoxContainer selectionBox;
        
        public override void _Ready()
        {
            selectionBox = GetNode<HBoxContainer>("Selection");
        }
    }
}
