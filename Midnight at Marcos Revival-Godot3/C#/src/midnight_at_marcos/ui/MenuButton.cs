using Godot;

namespace MidnightatMarcosRevival.midnight_at_marcos.ui
{
    public class MenuButton : TextureButton
    {
        private AudioStreamPlayer audio;
    
        public override void _Ready()
        {
            audio = GetNode<AudioStreamPlayer>("Audio");
        }

        public void onMenuButtonMouseEntered()
        {
            audio.Play();
            
            var box = GetParent().GetParent<Menu>().selectionBox;
            box.Visible = true;
            box.RectPosition = RectGlobalPosition - new Vector2(40, 7);
        }
    }
}
