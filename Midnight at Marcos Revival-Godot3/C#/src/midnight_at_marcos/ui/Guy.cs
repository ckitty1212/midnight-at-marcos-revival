using Godot;

namespace MidnightatMarcosRevival.midnight_at_marcos.ui
{
    public class Guy : TextureRect
    {
        private RandomNumberGenerator numberGenerator = new RandomNumberGenerator();
        private RandomNumberGenerator numberGenerator2 = new RandomNumberGenerator();
        private RandomNumberGenerator randomFrame = new RandomNumberGenerator();
        [Export] private Texture texture1;
        [Export] private Texture texture2;
        [Export] private Texture texture3;
        [Export] private Texture texture4;
        private int f;

        public override void _Process(float delta)
        {
            if (f > 0)
            {
                numberGenerator2.Randomize();
                var bruh = numberGenerator2.RandfRange(1, 100);
                if (!(bruh <= 15)) return;
                Texture = texture1;
                f = 0;
            }
        }

        public void onTwitchTimerTimeout()
        {
            numberGenerator.Randomize();
            var bruh = numberGenerator.RandfRange(1, 100);
            if (!(bruh <= 5)) return;
            f = 1;
            if (f < 1) return;
            randomFrame.Randomize();
            var bruhhhg = randomFrame.RandiRange(1, 3);
            switch (bruhhhg)
            {
                case 1:
                    Texture = texture2;
                    break;
                case 2:
                    Texture = texture3;
                    break;
                case 3:
                    Texture = texture4;
                    break;
            }

        }
    }
}
